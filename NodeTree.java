package Entities;

import IntelligentSystem.StateSpace;

/**
 * Class that represents every node of the tree search.
 * @author Ruth Rodriguez-Manzaneque Lopez, Lucia Calzado Piedrabuena and Lydia Prado Ibañez
 * Version: 1.0
 */
public class NodeTree implements Comparable<NodeTree>{
	
	private int [] state;
	private int cost;
	private NodeTree parent;
	private int depth;
	private int value;
	private String action;
	
	/**
	 * Constructor that only creates the state of the node.
	 * @param pieces
	 */
	public NodeTree(int [] pieces) {
		this.state = pieces;
	}
	
	/**
	 * Constructor that creates a complete node with all its characteristics.
	 * @param state Current state of the node.
	 * @param parent Parent of the node.
	 * @param strategy Strategy selected.
	 * @param action Action that the node perform.
	 */
	public NodeTree(int [] state, NodeTree parent, int strategy, String action) {
		this.depth = parent.depth + 1;		
		this.state = state;
		this.parent = parent;
		this.cost = parent.cost + 1;
		this.action = action;
		if(strategy == 1){
			this.value = this.depth;
		}else if(strategy == 2 || strategy == 3 || strategy == 5){
			this.value = -(this.depth);
		}else if(strategy == 4){
			this.value = this.cost;
		}else if(strategy == 6){
			this.value = this.cost + heuristic(state);
		}	
	}
	
	/**
	 * Compare the state with the goal state, count the tiles that are not in the right place.
	 * @param state
	 * @return Heuristic value
	 */
	private int heuristic(int [] state){
		int count_h = 0;		
		for(int i=0; i<state.length;i++){
			if(state[i]!=StateSpace.state_goal[i].getId()){
				count_h++;
			}
		}
		return count_h;
				
	}
	
	/**
	 * Get the state of the node.
	 * @return
	 */
	public int [] getState() {
		return state;
	}
	
	/**
	 * Change the state of the node.
	 * @param state
	 */
	public void setState(int [] state) {
		this.state = state;
	}
	
	/**
	 * Get the cost of the node.
	 * @return
	 */
	public int getCost() {
		return cost;
	}
	
	/**
	 * Change the cost of the node.
	 * @param cost
	 */
	public void setCost(int cost) {
		this.cost = cost;
	}
	
	/** 
	 * Get the parent of the current node.
	 * @return
	 */
	public NodeTree getParent() {
		return parent;
	}
	
	/**
	 * Change the parent of the current node.
	 * @param parent
	 */
	public void setParent(NodeTree parent) {
		this.parent = parent;
	}
	
	/**
	 * Get the depth of the current node.
	 * @return
	 */
	public int getDepth() {
		return depth;
	}
	
	/**
	 * Change the depth of the current node.
	 * @param depth
	 */
	public void setDepth(int depth) {
		this.depth = depth;
	}
	
	/**
	 * Get the value of the current node.
	 * @return
	 */
	public int getValue() {
		return value;
	}
	
	/**
	 * Change the value of the current node.
	 * @param value
	 */
	public void setValue(int value) {
		this.value = value;
	}
	
	/**
	 * Method to compare two nodes to know where to place it in the frontier.
	 */
	@Override
	public int compareTo(NodeTree node) {	
		if(this.value > node.getValue()){
			return 1;
		}else if(this.value < node.getValue()){
			return -1;
		}else{
			return 0;
		}
	}	
	
	/**
	 * Method to print the state of the current node.
	 */
	public void print_state(){
		for(int i=0; i<state.length; i++){
			System.out.print("["+state[i]+"]");
			
		}
		System.out.println("");
	}
	
	/**
	 * Get the action of the current node.
	 * @return
	 */
	public String getAction() {
		return action;
	}

	/**
	 * Change the action of the current node.
	 * @param action
	 */
	public void setAction(String action) {
		this.action = action;
	}
	
	public String generate_path(){
		if(this.parent!=null)
		return this.getParent().generate_path()+this.action+"\t";
		else
		return "";
	}
}
