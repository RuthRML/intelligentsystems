package IntelligentSystem;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import Entities.NodeTree;
import Entities.Piece;

/**
 * Principal class with the main and principal methods.
 * @author Ruth Rodriguez-Manzaneque Lopez, Lucia Calzado Piedrabuena and Lydia Prado Ibañez
 * Version: 1.0
 */

public class Principal {
	
	static Scanner read = new Scanner(System.in);
//	static String route_img_ordered = "drone5x4.png";
//	static String route_img_disordered = "droneInitial5x4.png";
	static String route_img_ordered = "Imagen.png";
	static String route_img_disordered = "ImagenDes.png";
	static double time_start;
	static double time_end;
	static double time_total;	
	
	public static void main(String [] args) throws IOException, NoSuchAlgorithmException{
		
		int rows = 0, columns = 0, strategy = 0, max_depth = 0, increment = 0;	

		System.out.println("Introduce the number of rows: ");
		rows = read.nextInt();
		System.out.println("Introduce the number of columns: ");
		columns = read.nextInt();
		StateSpace space_state = new StateSpace(new File(route_img_ordered),rows, columns);
		space_state.print_state_goal();
		int [] initial_state = reconstruction(rows,columns, space_state.getState_goal());
		Problem prob = new Problem(space_state, initial_state);
		System.out.println("Which strategy do you want?");
		System.out.println("1. BFS");
		System.out.println("2. DFS");
		System.out.println("3. DLS");
		System.out.println("4. UCS");
		System.out.println("5. IDS");
		System.out.println("6. A*");
		strategy = read.nextInt();
		switch(strategy){
			case 1:
				time_start = System.currentTimeMillis();
				bounded_search(prob, strategy, 20);				
				break;
			case 2:
				time_start = System.currentTimeMillis();
				bounded_search(prob, strategy, 20);
				break;
			case 3:
				System.out.println("Which is the maximum depth?");
				max_depth = read.nextInt();
				time_start = System.currentTimeMillis();
				bounded_search(prob, strategy, max_depth);
				break;
			case 4:
				time_start = System.currentTimeMillis();
				bounded_search(prob, strategy, 20);
				break;
			case 5:
				System.out.println("Which is the incrementally depth?");
				increment = read.nextInt();
				time_start = System.currentTimeMillis();
				search(prob, strategy, 9999, increment);
				break;
			case 6:
				time_start = System.currentTimeMillis();
				bounded_search(prob, strategy, 20);
				break;
			default:
				System.out.println("Option not valid.");
		}
	}

	/**
	 * Method that performs the search of the incremental limited search.
	 * @param prob Problem object.
	 * @param strategy Strategy selected.
	 * @param max_depth Maximum depth of the tree search.
	 * @param inc_depth Incremental depth for the search.
	 * @return solution that indicates if it has been found or not.
	 */
	public static boolean search(Problem prob, int strategy, int max_depth, int inc_depth){
		int current_depth = inc_depth;
		boolean solution = false;
		while((!solution) && (current_depth <= max_depth)){
			solution = bounded_search(prob, strategy, inc_depth);
			current_depth = current_depth + inc_depth;
		}		
		return solution;		
	}

	/**
	 * Method that performs the search of BFS, DFS, UCS, DLS and A*.
	 * @param prob Problem object.
	 * @param strategy Strategy selected.
	 * @param max_depth Maximum depth.
	 * @return Solution that indicates if it has been found or not.
	 */
	private static boolean bounded_search(Problem prob, int strategy, int max_depth) {
		System.out.println("Starting search...");
		// Initial process
		Frontier frontier = new Frontier();
		NodeTree n_current = null;
		NodeTree n_initial = new NodeTree(prob.getInitial_state());
		n_initial.setCost(0);
		n_initial.setValue(0);
		n_initial.setDepth(0);
		frontier.insert(n_initial);
		int expanded_nodes = 1;
		boolean solution = false;
		// Search Loop
		while((!solution) && (!frontier.isEmpty())){
			n_current = frontier.removeFirst();
			expanded_nodes++;
			if(prob.isGoal(n_current.getState())){
				solution = true;
			}else{
				prob.successors(n_current.getState(), frontier, n_current, strategy);
			}
		}
		if(solution){
			time_end = System.currentTimeMillis();
			time_total = time_end - time_start;
			return create_solution(n_current, expanded_nodes, strategy);
		}else{
			return solution;
		}
	}
	
	
	/**
	 * Method that creates a list with the valid nodes of the successors list.
	 * @param ls Successors list.
	 * @param parent Current node that will be the parent.
	 * @param max_depth Maximum depth.
	 * @param strategy Strategy selected.
	 * @return List of the nodes to be introduced in the frontier.
	 */
//	private static List<NodeTree> create_nodetree_list(List<NodeTree> ln, List<int []> ls, NodeTree parent, int max_depth, int strategy){
//		int [] temp_state;
//			while(!ls.isEmpty()){
//				 temp_state = ls.remove(0);		
//				 if((temp_state != null) && (!(parent.getDepth() > max_depth))){
//					ln.add(new NodeTree(temp_state,parent, strategy, movements[i]));
//				}
//			}
//		return ln;
//		
//	}

	/**
	 * Method that creates a file where we can find the path followed to reach the solution, the time taken and the number of nodes.
	 * @param n_current Node from the count will be initialize.
	 * @return solution that indicates if the file was correctly created.
	 */
	private static boolean create_solution(NodeTree n_current, int expanded_nodes, int strategy) {
			String route = "C://Users/Ruth/workspace/IntelligentSystems/solution.txt";
			boolean solution = false;
			BufferedWriter bw;
			File file_sol = new File(route);
			String str = "Undefinded";
				try {
					solution = true;
					bw = new BufferedWriter(new FileWriter(file_sol));
					switch(strategy){
						case 1:
							str = "BFS";
							break;
						case 2:
							str = "DFS";
							break;
						case 3: 
							str = "DLS";
							break;
						case 4:
							str = "UCS";
							break;
						case 5:
							str = "ICS";
							break;
						case 6:
							str = "A*";
							break;
					}
					bw.write("Strategy:"+str);
					bw.newLine();
					bw.write("Cost:"+n_current.getCost());
					bw.newLine();
					bw.write("Time elapsed:"+(time_total/1000.00000));
					bw.newLine();
					bw.write("Spacial complexity:"+expanded_nodes);
					bw.newLine();
					bw.write("Path:");
					bw.newLine();
					bw.write(n_current.generate_path());
					bw.newLine();					
					bw.close();
					System.out.println("Solution generated.");
				} catch (IOException e) {
					e.printStackTrace();
				}
			//}
		return solution;
	}

	/**
	 * Method that create the disordered image representation.
	 * @param rows Number of rows of the puzzle.
	 * @param colms Number of columns of the puzzle.
	 * @param state Ordered image representation.
	 * @return The matrix that represents the disordered image.
	 */
	public static int [] reconstruction(int rows, int colms, Piece [] state){
		Image m = new Image(rows,colms);
		return m.reconstruction(route_img_disordered, state);
	}
	
	
}
