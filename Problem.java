package IntelligentSystem;

import Entities.NodeTree;

/**
 * Class that represents the resolution of the problem.
 * @author Ruth Rodriguez-Manzaneque Lopez, Lucia Calzado Piedrabuena and Lydia Prado Ibañez
 * Version: 1.0
 *
 */

public class Problem {

	private int [] initial_state;
	private StateSpace state_space;
	
	/**
	 * Constructor that creates a problem with its state space and its initial state.
	 * @param state_space The representation of the ordered image.
	 * @param initial_state The representation of the disordered image at the beginning.
	 */
	public Problem(StateSpace state_space, int [] initial_state){
		this.initial_state = initial_state;
		this.state_space = state_space;
	}
	
	/**
	 * Get the initial state.
	 * @return
	 */
	public int [] getInitial_state() {
		return initial_state;
	}

	/**
	 * Get the state space of the problem.
	 * @return
	 */
	public StateSpace getState_space() {
		return state_space;
	}

	/**
	 * Change the state space of the problem.
	 * @param state_space
	 */
	public void setState_space(StateSpace state_space) {
		this.state_space = state_space;
	}
	
	
	public boolean isGoal(int [] current_state){	
		return state_space.isGoal(current_state);		
	}

	public void successors(int [] state, Frontier frontier, NodeTree n_current, int strategy){
		state_space.successors(state, frontier, n_current, strategy);
	}
}

