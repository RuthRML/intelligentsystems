package IntelligentSystem;


import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import Entities.NodeTree;

/**
 * Class that represent the frontier of the tree search.
 * @author Ruth Rodriguez-Manzaneque Lopez, Lucia Calzado Piedrabuena and Lydia Prado Ibañez
 * Version: 1.0
 *
 */


public class Frontier{

	private List<NodeTree> frontier_q;
	
	/**
	 * Constructor that initialize the frontier.
	 */
	public Frontier(){
		this.frontier_q = new ArrayList<NodeTree>();
	}
	
	/**
	 * Method to insert a node into the frontier.
	 * @param node
	 */
	public void insert(NodeTree node){
			Collections.sort(frontier_q);
			frontier_q.add(node);		
	}
	
	/**
	 * Method to remove the first element of the frontier.
	 * @return
	 */
	public NodeTree removeFirst(){
		Collections.sort(frontier_q);
		return frontier_q.remove(0);
	}
	
	/**
	 * Method to know if the frontier is empty.
	 * @return
	 */
	public boolean isEmpty(){
		return frontier_q.isEmpty();
	}
	
//	/**
//	 * Method to insert a list of nodes in the frontier.
//	 * @param list
//	 */
//	public void insertList(List<NodeTree> list){
//		while(!list.isEmpty()){
//			frontier_q.add(list.remove(0));
//		}
//	}
}
