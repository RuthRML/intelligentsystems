package Entities;
import java.awt.image.BufferedImage;
/**
 ****************************************************************************************************************
 * Class name: Piece.
 * @author Luc�a Calzado Piedrabuena
 * @author Lydia Prado Ib��ez
 * @author Ruth Rodr�guez-Manzaneque L�pez
 * Release/Creation date: October 2016
 * Class version: v1.0
 * Class description: Class that represent a piece of the puzzle.
 *************/

public class Piece {
	
	int id;
	BufferedImage piece_of_img;
	
	/**
	 * Constructor of a piece.
	 * @param id: id of the piece.
	 * @param piece_of_img: image that contains the piece.
	 */
	public Piece(int id, BufferedImage piece_of_img) {
		this.id = id;
		this.piece_of_img = piece_of_img;
	}
	
	/**
	 * Method to get the ID of a piece.
	 * @return id.
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * Method to modify the ID of a piece.
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * Method to get the image that a piece contains. 
	 * @return
	 */
	public BufferedImage getPiece_of_img() {
		return piece_of_img;
	}
	
	/**
	 * Method to change the image that a piece contains.
	 * @param piece_of_img
	 */
	public void setPiece_of_img(BufferedImage piece_of_img) {
		this.piece_of_img = piece_of_img;
	}	
	
}
