package IntelligentSystem;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import Entities.NodeTree;
import Entities.Piece;

/**
 * Class that create the state space of the problem.
 * @author Ruth Rodriguez-Manzaneque Lopez, Lucia Calzado Piedrabuena and Lydia Prado IbaÃ±ez
 * Version: 1.0
 *
 */

public class StateSpace {
	
	public static Piece [] state_goal;
	private int rows;
	private int colms;
	private BufferedImage img_ordered;
	private int image_width = 0;
	private int image_height = 0;
	private int piece_width = 0;
	private int piece_heigth = 0;
	private int number_of_pieces = 0;
	
	
	/**
	 * Constructor of a state space with its rows, columns and the image to be represented.
	 * @param imgOrdered
	 * @param rows
	 * @param colms
	 */
	public StateSpace(File imgOrdered, int rows, int colms){
		this.colms = colms;
    	this.rows = rows;
    	imageCharge(imgOrdered);
    	fixSize(img_ordered);
    	blackPiece();
    	generate_state();
	}
	
	public int [] change_to_int(){
		int [] new_state = new int[state_goal.length];
		for(int i=0; i<state_goal.length; i++){
			new_state[i]=state_goal[i].getId();
		}
		return new_state;
	}
	
	/**
	 * Method to know if the solution has been reached or not.
	 * @param pieces
	 * @return
	 */
	public boolean isGoal(int [] current_state){
			for(int i=0;i<current_state.length;i++){
				if(state_goal[i].getId()!=current_state[i]){
					return false;
				}
			}
		return true;		
	}
	
	/**
	 * Method to calculate the successor of the black piece.
	 * @param pieces
	 * @return
	 */
	public String [] successors(int [] current_state, Frontier frontier, NodeTree n_current, int strategy){
		String [] movements = new String[4]; // 4 Movements		
		for(int i=0;i<current_state.length;i++){
			if(current_state[i]==0){
				candidate_pieces(i, movements);
				for(int j=0;j<movements.length;j++){
					if(movements[j]!=null){
						switch(movements[j]){
							case "Left":
								frontier.insert(new NodeTree(generate_new_state(current_state, i, movements[j]), n_current, strategy, "Left"));
								break;
							case "Up":
								frontier.insert(new NodeTree(generate_new_state(current_state, i, movements[j]), n_current, strategy, "Up"));
								break;
							case "Right":
								frontier.insert(new NodeTree(generate_new_state(current_state, i, movements[j]), n_current, strategy, "Right"));
								break;
							case "Down":
								frontier.insert(new NodeTree(generate_new_state(current_state, i, movements[j]), n_current, strategy, "Down"));
								break;
						}
					}
				}
			}
		}	
		return movements;
	}

	/**
	 * Method that perform the movement inside the puzzle.
	 * @param move Movement direction.
	 * @param position Position where the black piece will move.
	 * @param array Representation of the puzzle.
	 */
	private int [] generate_new_state(int [] state, int position, String action){
		int [] new_state = new int[number_of_pieces];
		for (int k=0;k<state.length;k++){
		        new_state[k] = state[k];
		    }
		int new_pos = -1;
		if(action.equals("Left")){
			new_pos = position-1;
		}else if(action.equals("Right")){
			new_pos = position+1;
		}else if(action.equals("Up")){
			new_pos = position-colms;
		}else if(action.equals("Down")){
			new_pos = position+colms;
		}if((new_pos < new_state.length) && (new_pos >= 0)){
			int temp;
			temp = new_state[position];
			new_state[position] = new_state[new_pos];
			new_state[new_pos] = temp;		
		}
		return new_state;
	}
	
	
	
	/**
	 * Method to know what moves are available.
	 * @param position
	 * @param movements
	 */
	private void candidate_pieces(int position, String [] movements){
		if(position % colms != 0)
			movements[0] = "Left";
		else
			movements[0] = "No";
		if(position % colms != (colms - 1))
			movements[1] = "Right";
		else
			movements[1] = "No";
		if((position / colms) != 0)
			movements[2] = "Up";
		else
			movements[2] = "No";
		if((position / colms) != (colms -1))
			movements[3] = "Down";
		else
			movements[3] = "No";
	}
	
	/**
	 * Method to charge the image inside the program.
	 * @param imageOrdered
	 */
	private void imageCharge(File imageOrdered){
		try{
			img_ordered = ImageIO.read(imageOrdered); 
		}catch(IOException e){
			System.out.println(e.getMessage());
		}
	}
	
	/**
	 * Method to draw the black piece.
	 */
	private void blackPiece(){	
		for(int i = 0; i<piece_width; i++){
			for(int j = 0; j<piece_heigth; j++){
				img_ordered.setRGB(i, j, Color.BLACK.getRGB());
			}
		}
	}
	
	/**
	 * Method to change the width and height of the image and calculate the size of a piece of the puzzle.
	 * @param imagen
	 */
	private void fixSize(BufferedImage imagen){
		int anchura = 0, altura = 0;
		image_width = imagen.getWidth();
        image_height = imagen.getHeight();
        
        if(image_width % colms != 0 || image_height % rows != 0){
        	anchura = image_width % colms;
        	altura = image_height % rows;
        	Graphics2D gr = imagen.createGraphics();
            gr.drawImage(imagen, 0, 0, image_width - anchura, image_height - altura, null);
            gr.dispose();
        	image_width = imagen.getWidth();
        	image_height = imagen.getHeight();
        }
        
		piece_width = image_width / colms;
		piece_heigth = image_height / rows;
		number_of_pieces = rows * colms;
	}
	
	/**
	 * Method that generates the representation of the state space.
	 */
	public void generate_state(){
		state_goal = new Piece[number_of_pieces];
		int counter = 0;
			for(int i = 0; i<rows; i++){
				for(int j = 0 ; j<colms; j++){
					
					BufferedImage temp=new BufferedImage(piece_width,piece_heigth,img_ordered.getType());;
					Graphics2D gr = temp.createGraphics();
	                gr.drawImage(img_ordered, 0, 0, piece_width, piece_heigth, piece_width*j, piece_heigth*i, piece_width*j+piece_width, piece_heigth*i+piece_heigth, null);
	                gr.dispose();
	                
					state_goal[counter] = new Piece(counter, temp);
					counter++;
				} // end for j
			} // end for i
	}

	/**
	 * Method to get the state.
	 * @return
	 */
	public Piece [] getState_goal() {
		return state_goal;
	}
	
	/**
	 * Change the state.
	 * @param state
	 */
	public void setState(Piece [] state) {
		state_goal = state;
	}
	
	/**
	 * Get the rows of the puzzle.
	 * @return
	 */
	public int getRows() {
		return rows;
	}
	
	/**
	 * Change the rows of the puzzle.
	 * @param rows
	 */
	public void setRows(int rows) {
		this.rows = rows;
	}
	
	/**
	 * Get the columns of the puzzle.
	 * @return
	 */
	public int getColms() {
		return colms;
	}

	/**
	 * Change the columns of the puzzle.
	 * @param colms
	 */
	public void setColms(int colms) {
		this.colms = colms;
	}
	
	/**
	 * Method to print the state.
	 */
	public void print_state_goal(){
		for(int i=0; i<state_goal.length; i++){
			System.out.print("["+state_goal[i].getId()+"]");
			
		}
		System.out.println("");
	}

}
