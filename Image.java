package IntelligentSystem;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import javax.imageio.ImageIO;

import Entities.Piece;

/**
 * Image class with methods to create the representation of the image.
 * @author Ruth Rodriguez-Manzaneque Lopez, Lucia Calzado Piedrabuena and Lydia Prado IbaÃ±ez
 * Version: 1.0 *
 */

public class Image{
	private int rows = 0;
	private int colms = 0;
	private int image_width = 0;
	private int image_height = 0;
	private int piece_width = 0;
	private int piece_heigth = 0;
	private int number_of_pieces = 0;
	private BufferedImage img_disordered;
	private Piece [] state_puzzle;
	private int initial_position;
	
	/**
	 * Constructor of a Image object.
	 * @param rows Number of rows of the puzzle.
	 * @param colms Number of columns of the puzzle.
	 */
	public Image(int rows, int colms){			
    	this.colms = colms;
    	this.rows = rows;
    }
	
	/**
	 * Method to get the disordered image.
	 * @return
	 */
	public BufferedImage getImg_disordered() {
		return img_disordered;
	}
	
	public int [] change_to_int(){
		int [] new_state = new int[state_puzzle.length];
		for(int i=0; i<state_puzzle.length; i++){
			new_state[i]=state_puzzle[i].getId();
		}
		return new_state;
	}
	
	public int [] reconstruction(String route_img_disordered, Piece [] state){
		imageCharge(new File(route_img_disordered));
		fixSize(img_disordered);
		try {
			generate_arrayPiecesDisordered(state);
			identifyBlackPiece();
			print_state_puzzle();
			return change_to_int();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Method to get the representation of the image.
	 * @return
	 */
	public Piece [] getArrayDisordered() {
		return state_puzzle;
	}

	/**
	 * Method to charge the file of the image into the program.
	 * @param image
	 */
	public void imageCharge(File image){
		try{
			img_disordered = ImageIO.read(image);
		}catch(IOException e){
			System.out.println(e.getMessage());
		}
	}
	
	/**
	 * Method to identify where it is the black piece in the puzzle.
	 */
	public void identifyBlackPiece(){
		for(int i=0; i<state_puzzle.length;i++){
			if(state_puzzle[i].getId() == 0)
				setInitial_position(i);
		}
	}
	
	/**
	 * Method to change the width and the height of the image and calculate the sizes of every piece of the puzzle.
	 * @param imagen
	 */
	public void fixSize(BufferedImage imagen){
		int anchura = 0, altura = 0;
		image_width = imagen.getWidth();
        image_height = imagen.getHeight();
        
        if(image_width % colms != 0 || image_height % rows != 0){
        	anchura = image_width % colms;
        	altura = image_height % rows;
        	Graphics2D gr = imagen.createGraphics();
            gr.drawImage(imagen, 0, 0, image_width - anchura, image_height - altura, null);
            gr.dispose();
        	image_width = imagen.getWidth();
        	image_height = imagen.getHeight();
        }
        
		piece_width = image_width / colms;
		piece_heigth = image_height / rows;
		number_of_pieces = rows * colms;
	}
	
	/**
	 * Method to create the representation of the disordered image comparing it with the ordered representation.
	 * @param ordered Representation of the ordered image.
	 * @return
	 * @throws NoSuchAlgorithmException
	 * @throws IOException
	 */
	public Piece [] generate_arrayPiecesDisordered(Piece [] ordered) throws NoSuchAlgorithmException, IOException{
		state_puzzle = new Piece[number_of_pieces];
		int counter = 0;
		
			for(int i = 0; i<rows; i++){
				for(int j = 0 ; j<colms; j++){
					BufferedImage temp = new BufferedImage(piece_width,piece_heigth,img_disordered.getType());;
					Graphics2D gr = temp.createGraphics();
	                gr.drawImage(img_disordered, 0, 0, piece_width, piece_heigth, piece_width*j, piece_heigth*i, piece_width*j+piece_width, piece_heigth*i+piece_heigth, null);
	                gr.dispose();
	                
	                state_puzzle[counter] = new Piece(comparePieces(temp, ordered), temp);
					counter++;				
				} // end for j
			} // end for i
			return state_puzzle;
	}	
	
	/**
	 * Method that compares every piece of image to know its id.
	 * @param temporal_img Piece of image.
	 * @param ordered Representation of the ordered image.
	 * @return
	 */
	private int comparePieces(BufferedImage temporal_img, Piece [] ordered){
		
		int w1 = temporal_img.getWidth();
		int h1 = temporal_img.getHeight();
		boolean comp;
		
		for(Piece p: ordered){
			comp = true;
			BufferedImage ordered_img = p.getPiece_of_img();
			
			for (int y = 0; y < h1; y++) {
			      for (int x = 0; x < w1; x++) {
			        if (temporal_img.getRGB(x, y) != ordered_img.getRGB(x, y)) {
			        	comp = false;
			        	break;
			        }
			      }
			    }
			if(comp){
				return p.getId();
			}
		}
		return -1;
	}

	/**
	 * Method to get the initial position of the puzzle.
	 * @return
	 */
	public int getInitial_position() {
		return initial_position;
	}
	
	/**
	 * Method to change the initial position of the puzzle.
	 * @param initial_position
	 */
	public void setInitial_position(int initial_pos) {
		initial_position = initial_pos;
	}
	
	/**
	 * Method to print the representation.
	 */
	public void print_state_puzzle(){
		for(int i=0; i<state_puzzle.length; i++){
			System.out.print("["+state_puzzle[i].getId()+"]");
			
		}
		System.out.println("");
	}

}

